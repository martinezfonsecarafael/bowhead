#!/usr/bin/env python3
import os
import sys
import easydev
import shutil
sys.path.insert(0, os.path.abspath('../'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.doctest',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
    'easydev.copybutton',
]

html_static_path = ['_static']

source_suffix = '.rst'
master_doc = 'index'

project = 'bowhead'
copyright = '2017, Mathias Engel'
author = 'Mathias Engel'
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
pygments_style = 'sphinx'
napoleon_use_ivar = True  # to show Class Attributes correctly with napoleon

# -- Options for html output ------
html_theme = 'alabaster'
htmlhelp_basename = 'bowheaddoc'
html_logo = 'logo/logo.png'
html_favicon = 'logo/favicon.ico'
latex_logo = 'logo/logo.png'
html_sidebars = {
   '**': ['localtoc.html', 'searchbox.html']
}
